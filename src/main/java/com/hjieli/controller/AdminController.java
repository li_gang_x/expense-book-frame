package com.hjieli.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.hjieli.entity.SysMenuInfo;
import com.hjieli.entity.SysRoleXMenu;
import com.hjieli.entity.SysUserInfo;
import com.hjieli.entity.SysUserXRole;
import com.hjieli.service.SysMenuInfoService;
import com.hjieli.service.SysRoleXMenuService;
import com.hjieli.service.SysUserInfoService;
import com.hjieli.service.SysUserXRoleService;

@Controller
@RequestMapping("admin")
public class AdminController {
	
	@Autowired
	private SysUserInfoService sysUserInfoService;
	
	@Autowired
	private SysUserXRoleService sysUserXRoleService;
	
	@Autowired
	private SysRoleXMenuService sysRoleXMenuService;
	
	@Autowired
	private SysMenuInfoService sysMenuInfoService;

	@RequestMapping("index")
	public ModelAndView index(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("admin/index");
		Principal principal = request.getUserPrincipal();
		
		String account = principal.getName();
		
		//查询用户信息
		QueryWrapper<SysUserInfo> queryWrapper = new QueryWrapper<SysUserInfo>();
		queryWrapper.eq("account", account).eq("is_enable", "1");;
		SysUserInfo sysUserInfo = sysUserInfoService.getOne(queryWrapper);
		String userId = sysUserInfo.getId();
		
		//根据用户查询角色
		QueryWrapper<SysUserXRole> sysUserXRoleQueryWrapper = new QueryWrapper<SysUserXRole>();
		sysUserXRoleQueryWrapper.eq("user_id", userId).eq("is_enable", "1");;
		List<SysUserXRole> sysUserXRoleList = sysUserXRoleService.list(sysUserXRoleQueryWrapper);
		
		//查询角色权限信息
		QueryWrapper<SysRoleXMenu> sysRoleXMenuQueryWrapper = new QueryWrapper<SysRoleXMenu>();
		String roleIds = "";
		for(int index = 0 ; index < sysUserXRoleList.size(); index ++) {
			if(index >0 ) {
				roleIds += ",";
			}
			roleIds += sysUserXRoleList.get(index).getRoleId();
		}
		sysRoleXMenuQueryWrapper.in("role_id", roleIds.split(",")).eq("is_enable", "1");;
		List<SysRoleXMenu> sysRoleXMenuList = sysRoleXMenuService.list(sysRoleXMenuQueryWrapper);
		
		//查询权限信息
		QueryWrapper<SysMenuInfo> menuInfoWrapper = new QueryWrapper<SysMenuInfo>();
		String menuIds = "";
		for(int index = 0 ; index < sysRoleXMenuList.size(); index ++) {
			if(index >0 ) {
				menuIds += ",";
			}
			menuIds += sysRoleXMenuList.get(index).getMenuId();
		}
		menuInfoWrapper.in("id", menuIds.split(",")).eq("is_enable", "1");
		
		List<SysMenuInfo> menuList = sysMenuInfoService.list(menuInfoWrapper);
	
		JsonArray jsonArray = tree("0",menuList);
		modelAndView.addObject("menuList", jsonArray);
		
		modelAndView.addObject("sysUserInfo", sysUserInfo);
		
		return modelAndView;
	}

	@RequestMapping("workbench")
	public  ModelAndView workbench(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("admin/workbench");
		return modelAndView;
	}
	
	public JsonArray tree(String pid,List<SysMenuInfo> sourceList) {
		Gson gson = new Gson();
		JsonArray jsonArray = new JsonArray();
		for(SysMenuInfo menuInfo : sourceList) {
			if(menuInfo.getMenuPid().equals(pid)) {
				String jsonStr = gson.toJson(menuInfo);
				JsonElement jsonElement = JsonParser.parseString(jsonStr);
				JsonObject jsonObject = jsonElement.getAsJsonObject();
				JsonArray childMenu = tree(menuInfo.getId(),sourceList);
				if(childMenu != null) {
					jsonObject.add("arrays", JsonParser.parseString(gson.toJson(childMenu)).getAsJsonArray());
				}
				jsonArray.add(jsonObject);
			}
		}
		return jsonArray;
		
	}
}
