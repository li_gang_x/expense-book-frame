/*
* @author haojieli
* CreateDate 2020-12-17
* Copyright (c) 2020 haojieli
*/
package com.hjieli.controller.admin.sys;

import java.util.List;

import java.util.Map;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RestController;
import com.hjieli.entity.SysRoleXMenu;
import com.hjieli.service.SysRoleXMenuService; 
import org.springframework.web.bind.annotation.RestController;


/**
*
* @author haojieli
* @since 2020-12-17
*/
@RestController
@RequestMapping("admin/sys/sysRoleXMenu")
public class SysRoleXMenuController {

    @Autowired
    private SysRoleXMenuService sysRoleXMenuService;

    /**
     *   sys_role_x_menu表的添加页面
    **/
	@RequestMapping("add")
	public ModelAndView add(String id, HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("admin/sys/sysRoleXMenu/view");
		if(id == null) {
			modelAndView.addObject("sysRoleXMenu", new SysRoleXMenu());
		}else {
			QueryWrapper<SysRoleXMenu> wrapper = new QueryWrapper<>();
			wrapper.eq("id", id);
			SysRoleXMenu sysRoleXMenu = sysRoleXMenuService.getOne(wrapper);
			modelAndView.addObject("sysRoleXMenu", sysRoleXMenu);
		}
		return modelAndView;
	}
    /**
     *   保存sys_role_x_menu表的数据
    **/
    @RequestMapping(value = "save")
	public String save(@RequestBody Map<String,String> queryParams, HttpServletRequest request) {
        SysRoleXMenu sysRoleXMenu = new SysRoleXMenu();
        String[] menuIds = null;
        if(queryParams.containsKey("menuIds") && !queryParams.get("menuIds").toString().equals("")){
        	menuIds = queryParams.get("menuIds").toString().split(",");
        }
        if(queryParams.containsKey("roleId") && !queryParams.get("roleId").toString().equals("")){
            sysRoleXMenu.setRoleId(queryParams.get("roleId").toString());
        }
    
        //删除指定角色的权限
		QueryWrapper<SysRoleXMenu> wrapper = new QueryWrapper<>();
		wrapper.eq("role_id", sysRoleXMenu.getRoleId());
		sysRoleXMenuService.remove(wrapper);
		
		
		for(int index = 0; index < menuIds.length; index ++) {
			sysRoleXMenu = new SysRoleXMenu();
			sysRoleXMenu.setRoleId(queryParams.get("roleId").toString());
			sysRoleXMenu.setMenuId(menuIds[index].toString());
			sysRoleXMenu.setIsEnable(1);
			sysRoleXMenu.setCreateTime(LocalDateTime.now());
			sysRoleXMenuService.save(sysRoleXMenu);
		}
            
       
		return "success";
	}
    /**
     *   更新sys_role_x_menu表的数据
    **/
    @RequestMapping(value = "update")
	public String update(@RequestBody SysRoleXMenu sysRoleXMenu, HttpServletRequest request) {
		
		sysRoleXMenuService.updateById(sysRoleXMenu);
		return "success";
	}


    /**
     *   删除sys_role_x_menu表的数据
    **/
    @RequestMapping("delete")
	public String delete(@RequestBody Map<String,String> params, HttpServletRequest request) {
		
		String ids = params.get("ids").toString();
		//查询文章信息
		QueryWrapper<SysRoleXMenu> wrapper = new QueryWrapper<>();
		wrapper.in("id", ids.split(","));
		
	    SysRoleXMenu sysRoleXMenu = new SysRoleXMenu();
		sysRoleXMenu.setIsEnable(0);
		sysRoleXMenu.setCreateTime(LocalDateTime.now());
		sysRoleXMenuService.update(sysRoleXMenu,wrapper);

		return "success";
	}
    

    
    /**
     *   到sys_role_x_menu表的列表页面
    **/
	@RequestMapping("list")
	public ModelAndView list(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("admin/sys/sysRoleXMenu/list");
		return modelAndView;
	}

    /**
     *  查询sys_role_x_menu表的数据列表
    **/
    @RequestMapping("list/data")
	public String listdata(Integer page, Integer rows,
                            String id,
                            String roleId,
                            String menuId,
                            LocalDateTime createTime,
                            LocalDateTime updateTime,
                            Integer isEnable,
                            HttpServletRequest request) {
		
		if(page == null || page < 0) {
			page = 1;
		}
		if(rows == null || rows < 10) {
			rows = 10;
		} else if(rows > 50) {
			rows = 50;
		}
		Page<SysRoleXMenu> pageObj = new Page(page,rows);          //1表示当前页，而10表示每页的显示显示的条目数

        QueryWrapper<SysRoleXMenu> queryWrapper = new QueryWrapper();
		queryWrapper.eq(true,"is_enable", 1);
        
            if(id != null && !id.equals("")) {
                queryWrapper.eq(true,"id", id);
            }
            if(roleId != null && !roleId.equals("")) {
                queryWrapper.eq(true,"role_id", roleId);
            }
            if(menuId != null && !menuId.equals("")) {
                queryWrapper.eq(true,"menu_id", menuId);
            }
            if(createTime != null && !createTime.equals("")) {
                queryWrapper.eq(true,"create_time", createTime);
            }
            if(updateTime != null && !updateTime.equals("")) {
                queryWrapper.eq(true,"update_time", updateTime);
            }
            if(isEnable != null && !isEnable.equals("")) {
                queryWrapper.eq(true,"is_enable", isEnable);
            }
		queryWrapper.orderByDesc("create_time");

		
		Page<SysRoleXMenu> result = sysRoleXMenuService.listByCondition(pageObj, queryWrapper);
		Gson gson = new Gson();
		String dataStr = gson.toJson(result);
		JsonElement jsonElement = JsonParser.parseString(dataStr);
		
		JsonObject jsonObject = jsonElement.getAsJsonObject();
		jsonObject.remove("isAsc");
		jsonObject.remove("limit");
		jsonObject.remove("offset");
		jsonObject.remove("openSort");
		jsonObject.remove("optimizeCountSql");
		jsonObject.remove("searchCount");
		jsonObject.add("rows", jsonObject.get("records").getAsJsonArray());
		jsonObject.remove("records");
		return jsonObject.toString();
	}

}

