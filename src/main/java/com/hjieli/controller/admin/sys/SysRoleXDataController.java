/*
* @author haojieli
* CreateDate 2020-12-08
* Copyright (c) 2020 haojieli
*/
package com.hjieli.controller.admin.sys;

import java.util.List;

import java.util.Map;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RestController;
import com.hjieli.entity.SysRoleXData;
import com.hjieli.service.SysRoleXDataService; 
import org.springframework.web.bind.annotation.RestController;


/**
*
* @author haojieli
* @since 2020-12-08
*/
@RestController
@RequestMapping("admin/sys/sysRoleXData")
public class SysRoleXDataController {

    @Autowired
    private SysRoleXDataService sysRoleXDataService;

    /**
     *   sys_role_x_data表的添加页面
    **/
	@RequestMapping("add")
	public ModelAndView add(String id, HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("admin/sys/sysRoleXData/view");
		if(id == null) {
			modelAndView.addObject("articleInfo", new SysRoleXData());
		}else {
			QueryWrapper<SysRoleXData> wrapper = new QueryWrapper<>();
			wrapper.eq("id", id);
			SysRoleXData sysRoleXData = sysRoleXDataService.getOne(wrapper);
			modelAndView.addObject("sysRoleXData", sysRoleXData);
		}
		return modelAndView;
	}
    /**
     *   保存sys_role_x_data表的数据
    **/
    @RequestMapping(value = "save")
	public String save(@RequestBody Map<String,String> queryParams, HttpServletRequest request) {
        SysRoleXData sysRoleXData = new SysRoleXData();
            if(queryParams.containsKey("id") && !queryParams.get("id").toString().equals("")){
                sysRoleXData.setId(queryParams.get("id").toString());
            }
            if(queryParams.containsKey("dataId") && !queryParams.get("dataId").toString().equals("")){
                sysRoleXData.setDataId(queryParams.get("dataId").toString());
            }
            if(queryParams.containsKey("roleId") && !queryParams.get("roleId").toString().equals("")){
                sysRoleXData.setRoleId(queryParams.get("roleId").toString());
            }
            if(queryParams.containsKey("createTime") && !queryParams.get("createTime").toString().equals("")){
            }
            if(queryParams.containsKey("updateTime") && !queryParams.get("updateTime").toString().equals("")){
            }
            if(queryParams.containsKey("isEnable") && !queryParams.get("isEnable").toString().equals("")){
                sysRoleXData.setIsEnable(Integer.parseInt(queryParams.get("isEnable")));
            }
        sysRoleXData.setIsEnable(1);
		sysRoleXData.setCreateTime(LocalDateTime.now());
		sysRoleXDataService.save(sysRoleXData);
		return "success";
	}
    /**
     *   更新sys_role_x_data表的数据
    **/
    @RequestMapping(value = "update")
	public String update(@RequestBody SysRoleXData sysRoleXData, HttpServletRequest request) {
		
		sysRoleXDataService.updateById(sysRoleXData);
		return "success";
	}


    /**
     *   删除sys_role_x_data表的数据
    **/
    @RequestMapping("delete")
	public String delete(@RequestBody Map<String,String> params, HttpServletRequest request) {
		
		String ids = params.get("ids").toString();
		//查询文章信息
		QueryWrapper<SysRoleXData> wrapper = new QueryWrapper<>();
		wrapper.in("id", ids.split(","));
		
	    SysRoleXData sysRoleXData = new SysRoleXData();
		sysRoleXData.setIsEnable(0);
		sysRoleXData.setCreateTime(LocalDateTime.now());
		sysRoleXDataService.update(sysRoleXData,wrapper);

		return "success";
	}
    
    
    /**
     *   到sys_role_x_data表的列表页面
    **/
	@RequestMapping("list")
	public ModelAndView list(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("admin/sys/sysRoleXData/list");
		return modelAndView;
	}

    /**
     *  查询sys_role_x_data表的数据列表
    **/
    @RequestMapping("list/data")
	public String listdata(Integer page, Integer rows,
                            String id,
                            String dateId,
                            String roleId,
                            LocalDateTime createTime,
                            LocalDateTime updateTime,
                            Integer isEnable,
                            HttpServletRequest request) {
		
		if(page == null || page < 0) {
			page = 1;
		}
		if(rows == null || rows < 10) {
			rows = 10;
		} else if(rows > 50) {
			rows = 50;
		}
		Page<SysRoleXData> pageObj = new Page(page,rows);          //1表示当前页，而10表示每页的显示显示的条目数

        QueryWrapper<SysRoleXData> queryWrapper = new QueryWrapper();
		queryWrapper.eq(true,"is_enable", 1);
        
            if(id != null && !id.equals("")) {
                queryWrapper.eq(true,"id", id);
            }
            if(dateId != null && !dateId.equals("")) {
                queryWrapper.eq(true,"date_id", dateId);
            }
            if(roleId != null && !roleId.equals("")) {
                queryWrapper.eq(true,"role_id", roleId);
            }
            if(createTime != null && !createTime.equals("")) {
                queryWrapper.eq(true,"create_time", createTime);
            }
            if(updateTime != null && !updateTime.equals("")) {
                queryWrapper.eq(true,"update_time", updateTime);
            }
            if(isEnable != null && !isEnable.equals("")) {
                queryWrapper.eq(true,"is_enable", isEnable);
            }
		queryWrapper.orderByDesc("create_time");

		
		Page<SysRoleXData> result = sysRoleXDataService.listByCondition(pageObj, queryWrapper);
		Gson gson = new Gson();
		String dataStr = gson.toJson(result);
		JsonElement jsonElement = JsonParser.parseString(dataStr);
		
		JsonObject jsonObject = jsonElement.getAsJsonObject();
		jsonObject.remove("isAsc");
		jsonObject.remove("limit");
		jsonObject.remove("offset");
		jsonObject.remove("openSort");
		jsonObject.remove("optimizeCountSql");
		jsonObject.remove("searchCount");
		jsonObject.add("rows", jsonObject.get("records").getAsJsonArray());
		jsonObject.remove("records");
		return jsonObject.toString();
	}

}

