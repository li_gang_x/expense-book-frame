/*
* @author haojieli
* CreateDate 2020-12-08
* Copyright (c) 2020 haojieli
*/
package com.hjieli.controller.admin.sys;

import java.util.List;

import java.util.Map;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RestController;
import com.hjieli.entity.SysMenuInfo;
import com.hjieli.service.SysMenuInfoService; 
import org.springframework.web.bind.annotation.RestController;


/**
*
* @author haojieli
* @since 2020-12-08
*/
@RestController
@RequestMapping("admin/sys/sysMenuInfo")
public class SysMenuInfoController {

    @Autowired
    private SysMenuInfoService sysMenuInfoService;

    /**
     *   sys_menu_info表的添加页面
    **/
	@RequestMapping("add")
	public ModelAndView add(String id, HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("admin/sys/sysMenuInfo/view");
		if(id == null) {
			modelAndView.addObject("sysMenuInfo", new SysMenuInfo());
		}else {
			QueryWrapper<SysMenuInfo> wrapper = new QueryWrapper<>();
			wrapper.eq("id", id);
			SysMenuInfo sysMenuInfo = sysMenuInfoService.getOne(wrapper);
			modelAndView.addObject("sysMenuInfo", sysMenuInfo);
		}
		return modelAndView;
	}
    /**
     *   保存sys_menu_info表的数据
    **/
    @RequestMapping(value = "save")
	public String save(@RequestBody Map<String,String> queryParams, HttpServletRequest request) {
        SysMenuInfo sysMenuInfo = new SysMenuInfo();
            if(queryParams.containsKey("id") && !queryParams.get("id").toString().equals("")){
                sysMenuInfo.setId(queryParams.get("id").toString());
            }
            if(queryParams.containsKey("menuName") && !queryParams.get("menuName").toString().equals("")){
                sysMenuInfo.setMenuName(queryParams.get("menuName").toString());
            }
            if(queryParams.containsKey("menuUrl") && !queryParams.get("menuUrl").toString().equals("")){
                sysMenuInfo.setMenuUrl(queryParams.get("menuUrl").toString());
            }
            if(queryParams.containsKey("menuIcon") && !queryParams.get("menuIcon").toString().equals("")){
                sysMenuInfo.setMenuIcon(queryParams.get("menuIcon").toString());
            }
            if(queryParams.containsKey("menuPid") && !queryParams.get("menuPid").toString().equals("")){
                sysMenuInfo.setMenuPid(queryParams.get("menuPid").toString());
            }
            if(queryParams.containsKey("createTime") && !queryParams.get("createTime").toString().equals("")){
            }
            if(queryParams.containsKey("updateTime") && !queryParams.get("updateTime").toString().equals("")){
            }
            if(queryParams.containsKey("isEnable") && !queryParams.get("isEnable").toString().equals("")){
                sysMenuInfo.setIsEnable(Integer.parseInt(queryParams.get("isEnable")));
            }
        sysMenuInfo.setIsEnable(1);
		sysMenuInfo.setCreateTime(LocalDateTime.now());
		sysMenuInfoService.save(sysMenuInfo);
		return "success";
	}
    /**
     *   更新sys_menu_info表的数据
    **/
    @RequestMapping(value = "update")
	public String update(@RequestBody SysMenuInfo sysMenuInfo, HttpServletRequest request) {
		
		sysMenuInfoService.updateById(sysMenuInfo);
		return "success";
	}


    /**
     *   删除sys_menu_info表的数据
    **/
    @RequestMapping("delete")
	public String delete(@RequestBody Map<String,String> params, HttpServletRequest request) {
		
		String ids = params.get("ids").toString();
		//查询文章信息
		QueryWrapper<SysMenuInfo> wrapper = new QueryWrapper<>();
		wrapper.in("id", ids.split(","));
		
	    SysMenuInfo sysMenuInfo = new SysMenuInfo();
		sysMenuInfo.setIsEnable(0);
		sysMenuInfo.setCreateTime(LocalDateTime.now());
		sysMenuInfoService.update(sysMenuInfo,wrapper);

		return "success";
	}
    
    
    /**
     *   到sys_menu_info表的列表页面
    **/
	@RequestMapping("list")
	public ModelAndView list(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("admin/sys/sysMenuInfo/list");
		return modelAndView;
	}

    /**
     *  查询sys_menu_info表的数据列表
    **/
    @RequestMapping("list/data")
	public String listdata(Integer page, Integer rows,
                            String id,
                            String menuName,
                            String menuUrl,
                            String menuIcon,
                            String menuPid,
                            LocalDateTime createTime,
                            LocalDateTime updateTime,
                            Integer isEnable,
                            HttpServletRequest request) {
		
		if(page == null || page < 0) {
			page = 1;
		}
		if(rows == null || rows < 10) {
			rows = 10;
		} else if(rows > 50) {
			rows = 50;
		}
		Page<SysMenuInfo> pageObj = new Page(page,rows);          //1表示当前页，而10表示每页的显示显示的条目数

        QueryWrapper<SysMenuInfo> queryWrapper = new QueryWrapper();
		queryWrapper.eq(true,"is_enable", 1);
        
            if(id != null && !id.equals("")) {
                queryWrapper.eq(true,"id", id);
            }
            if(menuName != null && !menuName.equals("")) {
                queryWrapper.eq(true,"menu_name", menuName);
            }
            if(menuUrl != null && !menuUrl.equals("")) {
                queryWrapper.eq(true,"menu_url", menuUrl);
            }
            if(menuIcon != null && !menuIcon.equals("")) {
                queryWrapper.eq(true,"menu_icon", menuIcon);
            }
            if(menuPid != null && !menuPid.equals("")) {
                queryWrapper.eq(true,"menu_pid", menuPid);
            }
            if(createTime != null && !createTime.equals("")) {
                queryWrapper.eq(true,"create_time", createTime);
            }
            if(updateTime != null && !updateTime.equals("")) {
                queryWrapper.eq(true,"update_time", updateTime);
            }
            if(isEnable != null && !isEnable.equals("")) {
                queryWrapper.eq(true,"is_enable", isEnable);
            }
		queryWrapper.orderByDesc("create_time");

		
		Page<SysMenuInfo> result = sysMenuInfoService.listByCondition(pageObj, queryWrapper);
		Gson gson = new Gson();
		String dataStr = gson.toJson(result);
		JsonElement jsonElement = JsonParser.parseString(dataStr);
		
		JsonObject jsonObject = jsonElement.getAsJsonObject();
		jsonObject.remove("isAsc");
		jsonObject.remove("limit");
		jsonObject.remove("offset");
		jsonObject.remove("openSort");
		jsonObject.remove("optimizeCountSql");
		jsonObject.remove("searchCount");
		jsonObject.add("rows", jsonObject.get("records").getAsJsonArray());
		jsonObject.remove("records");
		return jsonObject.toString();
	}
    
    @RequestMapping("list/treedata")
	public String listTreeData(HttpServletRequest request) {
    	QueryWrapper<SysMenuInfo> wrapper = new QueryWrapper<SysMenuInfo>();
    	wrapper.eq("is_enable", "1");
		List<SysMenuInfo> menuList = sysMenuInfoService.list(wrapper);
		JsonArray jsonArray = tree("0",menuList);
		
		
		return jsonArray.toString();
	}
    
    public JsonArray tree(String pid,List<SysMenuInfo> sourceList) {
		Gson gson = new Gson();
		JsonArray jsonArray = new JsonArray();
		for(SysMenuInfo menuInfo : sourceList) {
			if(menuInfo.getMenuPid().equals(pid)) {
				
				JsonObject jsonObject = new JsonObject();
				jsonObject.addProperty("id", menuInfo.getId());
				jsonObject.addProperty("text", menuInfo.getMenuName());
				JsonArray childMenu = tree(menuInfo.getId(),sourceList);
				if(childMenu != null && childMenu.size() > 0) {
					jsonObject.add("children", JsonParser.parseString(gson.toJson(childMenu)).getAsJsonArray());
				}
				jsonArray.add(jsonObject);
			}
		}
		return jsonArray;
		
	}

}

