/*
* @author haojieli
* CreateDate 2020-12-08
* Copyright (c) 2020 haojieli
*/
package com.hjieli.controller.admin.sys;

import java.util.List;

import java.util.Map;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RestController;
import com.hjieli.entity.UserLoginInfo;
import com.hjieli.service.UserLoginInfoService; 
import org.springframework.web.bind.annotation.RestController;


/**
*
* @author haojieli
* @since 2020-12-08
*/
@RestController
@RequestMapping("admin/sys/userLoginInfo")
public class UserLoginInfoController {

    @Autowired
    private UserLoginInfoService userLoginInfoService;

    /**
     *   user_login_info表的添加页面
    **/
	@RequestMapping("add")
	public ModelAndView add(String id, HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("admin/sys/userLoginInfo/view");
		if(id == null) {
			modelAndView.addObject("userLoginInfo", new UserLoginInfo());
		}else {
			QueryWrapper<UserLoginInfo> wrapper = new QueryWrapper<>();
			wrapper.eq("id", id);
			UserLoginInfo userLoginInfo = userLoginInfoService.getOne(wrapper);
			modelAndView.addObject("userLoginInfo", userLoginInfo);
		}
		return modelAndView;
	}
    /**
     *   保存user_login_info表的数据
    **/
    @RequestMapping(value = "save")
	public String save(@RequestBody Map<String,String> queryParams, HttpServletRequest request) {
        UserLoginInfo userLoginInfo = new UserLoginInfo();
            if(queryParams.containsKey("id") && !queryParams.get("id").toString().equals("")){
                userLoginInfo.setId(queryParams.get("id").toString());
            }
            if(queryParams.containsKey("userId") && !queryParams.get("userId").toString().equals("")){
                userLoginInfo.setUserId(queryParams.get("userId").toString());
            }
            if(queryParams.containsKey("userName") && !queryParams.get("userName").toString().equals("")){
                userLoginInfo.setUserName(queryParams.get("userName").toString());
            }
            if(queryParams.containsKey("loginTime") && !queryParams.get("loginTime").toString().equals("")){
            }
            if(queryParams.containsKey("loginIp") && !queryParams.get("loginIp").toString().equals("")){
                userLoginInfo.setLoginIp(queryParams.get("loginIp").toString());
            }
            if(queryParams.containsKey("loginMachine") && !queryParams.get("loginMachine").toString().equals("")){
                userLoginInfo.setLoginMachine(queryParams.get("loginMachine").toString());
            }
            if(queryParams.containsKey("loginAddress") && !queryParams.get("loginAddress").toString().equals("")){
                userLoginInfo.setLoginAddress(queryParams.get("loginAddress").toString());
            }
            if(queryParams.containsKey("createTime") && !queryParams.get("createTime").toString().equals("")){
            }
            if(queryParams.containsKey("updateTime") && !queryParams.get("updateTime").toString().equals("")){
            }
            if(queryParams.containsKey("isEnable") && !queryParams.get("isEnable").toString().equals("")){
                userLoginInfo.setIsEnable(Integer.parseInt(queryParams.get("isEnable")));
            }
        userLoginInfo.setIsEnable(1);
		userLoginInfo.setCreateTime(LocalDateTime.now());
		userLoginInfoService.save(userLoginInfo);
		return "success";
	}
    /**
     *   更新user_login_info表的数据
    **/
    @RequestMapping(value = "update")
	public String update(@RequestBody UserLoginInfo userLoginInfo, HttpServletRequest request) {
		
		userLoginInfoService.updateById(userLoginInfo);
		return "success";
	}


    /**
     *   删除user_login_info表的数据
    **/
    @RequestMapping("delete")
	public String delete(@RequestBody Map<String,String> params, HttpServletRequest request) {
		
		String ids = params.get("ids").toString();
		//查询文章信息
		QueryWrapper<UserLoginInfo> wrapper = new QueryWrapper<>();
		wrapper.in("id", ids.split(","));
		
	    UserLoginInfo userLoginInfo = new UserLoginInfo();
		userLoginInfo.setIsEnable(0);
		userLoginInfo.setCreateTime(LocalDateTime.now());
		userLoginInfoService.update(userLoginInfo,wrapper);

		return "success";
	}
    
    
    /**
     *   到user_login_info表的列表页面
    **/
	@RequestMapping("list")
	public ModelAndView list(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("admin/sys/userLoginInfo/list");
		return modelAndView;
	}

    /**
     *  查询user_login_info表的数据列表
    **/
    @RequestMapping("list/data")
	public String listdata(Integer page, Integer rows,
                            String id,
                            String userId,
                            String userName,
                            LocalDateTime loginTime,
                            String loginIp,
                            String loginMachine,
                            String loginAddress,
                            LocalDateTime createTime,
                            LocalDateTime updateTime,
                            Integer isEnable,
                            HttpServletRequest request) {
		
		if(page == null || page < 0) {
			page = 1;
		}
		if(rows == null || rows < 10) {
			rows = 10;
		} else if(rows > 50) {
			rows = 50;
		}
		Page<UserLoginInfo> pageObj = new Page(page,rows);          //1表示当前页，而10表示每页的显示显示的条目数

        QueryWrapper<UserLoginInfo> queryWrapper = new QueryWrapper();
		queryWrapper.eq(true,"is_enable", 1);
        
            if(id != null && !id.equals("")) {
                queryWrapper.eq(true,"id", id);
            }
            if(userId != null && !userId.equals("")) {
                queryWrapper.eq(true,"user_id", userId);
            }
            if(userName != null && !userName.equals("")) {
                queryWrapper.eq(true,"user_name", userName);
            }
            if(loginTime != null && !loginTime.equals("")) {
                queryWrapper.eq(true,"login_time", loginTime);
            }
            if(loginIp != null && !loginIp.equals("")) {
                queryWrapper.eq(true,"login_ip", loginIp);
            }
            if(loginMachine != null && !loginMachine.equals("")) {
                queryWrapper.eq(true,"login_machine", loginMachine);
            }
            if(loginAddress != null && !loginAddress.equals("")) {
                queryWrapper.eq(true,"login_address", loginAddress);
            }
            if(createTime != null && !createTime.equals("")) {
                queryWrapper.eq(true,"create_time", createTime);
            }
            if(updateTime != null && !updateTime.equals("")) {
                queryWrapper.eq(true,"update_time", updateTime);
            }
            if(isEnable != null && !isEnable.equals("")) {
                queryWrapper.eq(true,"is_enable", isEnable);
            }
		queryWrapper.orderByDesc("create_time");

		
		Page<UserLoginInfo> result = userLoginInfoService.listByCondition(pageObj, queryWrapper);
		Gson gson = new Gson();
		String dataStr = gson.toJson(result);
		JsonElement jsonElement = JsonParser.parseString(dataStr);
		
		JsonObject jsonObject = jsonElement.getAsJsonObject();
		jsonObject.remove("isAsc");
		jsonObject.remove("limit");
		jsonObject.remove("offset");
		jsonObject.remove("openSort");
		jsonObject.remove("optimizeCountSql");
		jsonObject.remove("searchCount");
		jsonObject.add("rows", jsonObject.get("records").getAsJsonArray());
		jsonObject.remove("records");
		return jsonObject.toString();
	}

}

