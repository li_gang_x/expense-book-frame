package com.hjieli.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.JsonArray;

public class BaseController {


	public Map<String,Object> returnResult(String errorno,String msg,Object data){
		Map<String,Object> resultMap = new HashMap<String,Object>();
		resultMap.put("errorno", errorno);
		resultMap.put("msg", msg);
		resultMap.put("data", data);
		
		return resultMap;
	}
	
	public Map<String,Object> returnResult(String errorno, String msg, List<Map<String,Object>> data){
		return returnResult(errorno, msg, data, false);
	}
	
	public Map<String,Object> returnResult(String errorno, String msg, List<Map<String,Object>> data, boolean isDate){
		String dateFormat = "yyyy-MM-dd HH:mm:ss";
		if(isDate) {
			dateFormat = "yyyy-MM-dd";
		}
		return returnResult(errorno, msg, data, dateFormat);
	}
	
	public Map<String,Object> returnResult(String errorno, String msg, List<Map<String,Object>> data, String dateFormat){
		
		Map<String,Object> resultMap = new HashMap<String,Object>();
		resultMap.put("errorno", errorno);
		resultMap.put("msg", msg);
//		
//		Map<String,Object> tempMap = null;
//		JSONObject tempJsonObj = null;
//		Object tempObj = null;
//		String tempKey = "";
//		JSONArray jsonArray = new JSONArray();
//		for(int index = 0; index < data.size(); index ++) {
//			tempMap = data.get(index);
//			tempJsonObj = new JSONObject();
//			for(String key : tempMap.keySet()) {
//				tempKey = StringUtil.toCamel(key);
//				tempObj = tempMap.get(key);
//				if(tempKey.equals("createTime") ||tempKey.equals("updateTime")) {
//					tempObj = JSON.toJSONStringWithDateFormat(tempObj, dateFormat, SerializerFeature.WriteDateUseDateFormat); 
//					
//					tempObj = tempObj.toString().replaceAll("\"","");
//				}
//				tempJsonObj.put(tempKey, tempObj);
//			}
//			jsonArray.add(tempJsonObj);
//		}
		Object obj = new JsonArray();
		
		return returnResult(errorno, msg, obj);
	}
}
