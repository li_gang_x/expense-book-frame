/*
* @author haojieli
* CreateDate 2021-01-04
* Copyright (c) 2020 haojieli
*/
package com.hjieli.controller.api;

import java.util.List;

import java.util.Map;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RestController;
import com.hjieli.entity.ExpenseBookInfo;
import com.hjieli.service.ExpenseBookInfoService; 
import org.springframework.web.bind.annotation.RestController;


/**
*
* @author haojieli
* @since 2021-01-04
*/
@RestController
@RequestMapping("api/expenseBookInfo")
public class ExpenseBookInfoController {

    @Autowired
    private ExpenseBookInfoService expenseBookInfoService;

    /**
     *   expense_book_info表的添加页面
    **/
	@RequestMapping("add")
	public ModelAndView add(String id, HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("api/expenseBookInfo/view");
		if(id == null) {
			modelAndView.addObject("expenseBookInfo", new ExpenseBookInfo());
		}else {
			QueryWrapper<ExpenseBookInfo> wrapper = new QueryWrapper<>();
			wrapper.eq("id", id);
			ExpenseBookInfo expenseBookInfo = expenseBookInfoService.getOne(wrapper);
			modelAndView.addObject("expenseBookInfo", expenseBookInfo);
		}
		return modelAndView;
	}
	
	@RequestMapping("info")
	public String info(String id, HttpServletRequest request) {
		JsonObject resultObj = new JsonObject();
		if(id != null) {
			QueryWrapper<ExpenseBookInfo> wrapper = new QueryWrapper<>();
			wrapper.eq("id", id);

			ExpenseBookInfo expenseBookInfo = expenseBookInfoService.getOne(wrapper);
			Gson gson = new Gson();
			String json = gson.toJson(expenseBookInfo);
			JsonObject objJson = new JsonParser().parseString(json).getAsJsonObject();
			
			resultObj.addProperty("code", "200");
			resultObj.addProperty("msg", "获取成功");
			resultObj.add("data", objJson);
		}else {
			resultObj.addProperty("code", "201");
			resultObj.addProperty("msg", "获取失败，参数丢失");
			resultObj.addProperty("data", "");
		}
		return resultObj.toString();
	}
	
    /**
     *   保存expense_book_info表的数据
    **/
    @RequestMapping(value = "save")
	public String save(@RequestBody Map<String,String> queryParams, HttpServletRequest request) {
        ExpenseBookInfo expenseBookInfo = new ExpenseBookInfo();
            if(queryParams.containsKey("id") && !queryParams.get("id").toString().equals("")){
                expenseBookInfo.setId(queryParams.get("id").toString());
            }
            if(queryParams.containsKey("title") && !queryParams.get("title").toString().equals("")){
                expenseBookInfo.setTitle(queryParams.get("title").toString());
            }
            if(queryParams.containsKey("createTime") && !queryParams.get("createTime").toString().equals("")){
            }
            if(queryParams.containsKey("updateTime") && !queryParams.get("updateTime").toString().equals("")){
            }
            if(queryParams.containsKey("nowMonthPayOut") && !queryParams.get("nowMonthPayOut").toString().equals("")){
                expenseBookInfo.setNowMonthPayOut(queryParams.get("nowMonthPayOut").toString());
            }
            if(queryParams.containsKey("nowMonthPutIn") && !queryParams.get("nowMonthPutIn").toString().equals("")){
                expenseBookInfo.setNowMonthPutIn(queryParams.get("nowMonthPutIn").toString());
            }
            if(queryParams.containsKey("isEnable") && !queryParams.get("isEnable").toString().equals("")){
                expenseBookInfo.setIsEnable(Integer.parseInt(queryParams.get("isEnable")));
            }
            if(queryParams.containsKey("createUserId") && !queryParams.get("createUserId").toString().equals("")){
                expenseBookInfo.setCreateUserId(queryParams.get("createUserId").toString());
            }
        expenseBookInfo.setIsEnable(1);
		expenseBookInfo.setCreateTime(LocalDateTime.now());
		expenseBookInfoService.save(expenseBookInfo);
		return "success";
	}
    /**
     *   更新expense_book_info表的数据
    **/
    @RequestMapping(value = "update")
	public String update(@RequestBody ExpenseBookInfo expenseBookInfo, HttpServletRequest request) {
		
		expenseBookInfoService.updateById(expenseBookInfo);
		return "success";
	}


    /**
     *   删除expense_book_info表的数据
    **/
    @RequestMapping("delete")
	public String delete(@RequestBody Map<String,String> params, HttpServletRequest request) {
		
		String ids = params.get("ids").toString();
		//查询文章信息
		QueryWrapper<ExpenseBookInfo> wrapper = new QueryWrapper<>();
		wrapper.in("id", ids.split(","));
		
	    ExpenseBookInfo expenseBookInfo = new ExpenseBookInfo();
		expenseBookInfo.setIsEnable(0);
		expenseBookInfo.setCreateTime(LocalDateTime.now());
		expenseBookInfoService.update(expenseBookInfo,wrapper);

		return "success";
	}
    
    
    /**
     *   到expense_book_info表的列表页面
    **/
	@RequestMapping("list")
	public ModelAndView list(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("api/expenseBookInfo/list");
		return modelAndView;
	}

    /**
     *  查询expense_book_info表的数据列表
    **/
    @RequestMapping("list/data")
	public String listdata(Integer page, Integer rows,
                            String id,
                            String title,
                            LocalDateTime createTime,
                            LocalDateTime updateTime,
                            String nowMonthPayOut,
                            String nowMonthPutIn,
                            Integer isEnable,
                            String createUserId,
                            HttpServletRequest request) {
		
		if(page == null || page < 0) {
			page = 1;
		}
		if(rows == null || rows < 10) {
			rows = 10;
		} else if(rows > 50) {
			rows = 50;
		}
		Page<ExpenseBookInfo> pageObj = new Page(page,rows);          //1表示当前页，而10表示每页的显示显示的条目数

        QueryWrapper<ExpenseBookInfo> queryWrapper = new QueryWrapper();
		queryWrapper.eq(true,"is_enable", 1);
        
            if(id != null && !id.equals("")) {
                queryWrapper.eq(true,"id", id);
            }
            if(title != null && !title.equals("")) {
                queryWrapper.eq(true,"title", title);
            }
            if(createTime != null && !createTime.equals("")) {
                queryWrapper.eq(true,"create_time", createTime);
            }
            if(updateTime != null && !updateTime.equals("")) {
                queryWrapper.eq(true,"update_time", updateTime);
            }
            if(nowMonthPayOut != null && !nowMonthPayOut.equals("")) {
                queryWrapper.eq(true,"now_month_pay_out", nowMonthPayOut);
            }
            if(nowMonthPutIn != null && !nowMonthPutIn.equals("")) {
                queryWrapper.eq(true,"now_month_put_in", nowMonthPutIn);
            }
            if(isEnable != null && !isEnable.equals("")) {
                queryWrapper.eq(true,"is_enable", isEnable);
            }
            if(createUserId != null && !createUserId.equals("")) {
                queryWrapper.eq(true,"create_user_id", createUserId);
            }
		queryWrapper.orderByDesc("create_time");

		
		Page<ExpenseBookInfo> result = expenseBookInfoService.listByCondition(pageObj, queryWrapper);
		Gson gson = new Gson();
		String dataStr = gson.toJson(result);
		JsonElement jsonElement = JsonParser.parseString(dataStr);
		
		JsonObject jsonObject = jsonElement.getAsJsonObject();
		jsonObject.remove("isAsc");
		jsonObject.remove("limit");
		jsonObject.remove("offset");
		jsonObject.remove("openSort");
		jsonObject.remove("optimizeCountSql");
		jsonObject.remove("searchCount");
		jsonObject.add("rows", jsonObject.get("records").getAsJsonArray());
		jsonObject.remove("records");
		return jsonObject.toString();
	}

}

