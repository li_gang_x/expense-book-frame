/*
* @author haojieli
* CreateDate 2021-01-04
* Copyright (c) 2020 haojieli
*/
package com.hjieli.controller.api;

import java.util.List;

import java.util.Map;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RestController;

import com.hjieli.controller.BaseController;
import com.hjieli.entity.ExpenseBookDetail;
import com.hjieli.service.ExpenseBookDetailService; 
import org.springframework.web.bind.annotation.RestController;


/**
*
* @author haojieli
* @since 2021-01-04
*/
@RestController
@RequestMapping("api/expenseBookDetail")
public class ExpenseBookDetailController  extends BaseController{

    @Autowired
    private ExpenseBookDetailService expenseBookDetailService;

    /**
     *   expense_book_detail表的添加页面
    **/
	@RequestMapping("add")
	public ModelAndView add(String id, HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("api/expenseBookDetail/view");
		if(id == null) {
			modelAndView.addObject("expenseBookDetail", new ExpenseBookDetail());
		}else {
			QueryWrapper<ExpenseBookDetail> wrapper = new QueryWrapper<>();
			wrapper.eq("id", id);
			ExpenseBookDetail expenseBookDetail = expenseBookDetailService.getOne(wrapper);
			modelAndView.addObject("expenseBookDetail", expenseBookDetail);
		}
		return modelAndView;
	}
	 @RequestMapping(value = "info")
	public String info(String id){
		
		QueryWrapper<ExpenseBookDetail> wrapper = new QueryWrapper<>();
		wrapper.eq("id", id);
		wrapper.eq("is_enable", 1);
		ExpenseBookDetail expenseBookDetail = expenseBookDetailService.getOne(wrapper);
		Gson gson = new Gson();
		return gson.toJson(expenseBookDetail);
	}
	
    /**
     *   保存expense_book_detail表的数据
    **/
    @RequestMapping(value = "save")
	public String save(@RequestBody Map<String,String> queryParams, HttpServletRequest request) {
        ExpenseBookDetail expenseBookDetail = new ExpenseBookDetail();
            if(queryParams.containsKey("id") && !queryParams.get("id").toString().equals("")){
                expenseBookDetail.setId(queryParams.get("id").toString());
            }
            if(queryParams.containsKey("expenseBookId") && !queryParams.get("expenseBookId").toString().equals("")){
                expenseBookDetail.setExpenseBookId(queryParams.get("expenseBookId").toString());
            }
            if(queryParams.containsKey("expenseBookName") && !queryParams.get("expenseBookName").toString().equals("")){
                expenseBookDetail.setExpenseBookName(queryParams.get("expenseBookName").toString());
            }
            if(queryParams.containsKey("typeName") && !queryParams.get("typeName").toString().equals("")){
                expenseBookDetail.setTypeName(queryParams.get("typeName").toString());
            }
            if(queryParams.containsKey("typeId") && !queryParams.get("typeId").toString().equals("")){
                expenseBookDetail.setTypeId(queryParams.get("typeId").toString());
            }
            if(queryParams.containsKey("money") && !queryParams.get("money").toString().equals("")){
                expenseBookDetail.setMoney(queryParams.get("money").toString());
            }
            if(queryParams.containsKey("remark") && !queryParams.get("remark").toString().equals("")){
                expenseBookDetail.setRemark(queryParams.get("remark").toString());
            }
            if(queryParams.containsKey("attachment") && !queryParams.get("attachment").toString().equals("")){
                expenseBookDetail.setAttachment(queryParams.get("attachment").toString());
            }
            if(queryParams.containsKey("createTime") && !queryParams.get("createTime").toString().equals("")){
            }
            if(queryParams.containsKey("updateTime") && !queryParams.get("updateTime").toString().equals("")){
            }
            if(queryParams.containsKey("isEnable") && !queryParams.get("isEnable").toString().equals("")){
                expenseBookDetail.setIsEnable(Integer.parseInt(queryParams.get("isEnable")));
            }
            if(queryParams.containsKey("createUserId") && !queryParams.get("createUserId").toString().equals("")){
                expenseBookDetail.setCreateUserId(queryParams.get("createUserId").toString());
            }
            if(queryParams.containsKey("way") && !queryParams.get("way").toString().equals("")){
                expenseBookDetail.setWay(queryParams.get("way").toString());
            }
        expenseBookDetail.setIsEnable(1);
		expenseBookDetail.setCreateTime(LocalDateTime.now());
		expenseBookDetailService.save(expenseBookDetail);
		return "success";
	}
    /**
     *   更新expense_book_detail表的数据
    **/
    @RequestMapping(value = "update")
	public String update(@RequestBody ExpenseBookDetail expenseBookDetail, HttpServletRequest request) {
		
		expenseBookDetailService.updateById(expenseBookDetail);
		return "success";
	}


    /**
     *   删除expense_book_detail表的数据
    **/
    @RequestMapping("delete")
	public String delete(@RequestBody Map<String,String> params, HttpServletRequest request) {
		
		String ids = params.get("ids").toString();
		//查询文章信息
		QueryWrapper<ExpenseBookDetail> wrapper = new QueryWrapper<>();
		wrapper.in("id", ids.split(","));
		
	    ExpenseBookDetail expenseBookDetail = new ExpenseBookDetail();
		expenseBookDetail.setIsEnable(0);
		expenseBookDetail.setCreateTime(LocalDateTime.now());
		expenseBookDetailService.update(expenseBookDetail,wrapper);

		return "success";
	}
    
    
    /**
     *   到expense_book_detail表的列表页面
    **/
	@RequestMapping("list")
	public ModelAndView list(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("api/expenseBookDetail/list");
		return modelAndView;
	}

    /**
     *  查询expense_book_detail表的数据列表
    **/
    @RequestMapping("list/data")
	public String listdata(Integer page, Integer rows,
                            String id,
                            String manageMoneyBookId,
                            String manageMoneyBookName,
                            String typeName,
                            String typeId,
                            String money,
                            String remark,
                            String attachment,
                            LocalDateTime createTime,
                            LocalDateTime updateTime,
                            Integer isEnable,
                            String createUserId,
                            String way,
                            HttpServletRequest request) {
		
		if(page == null || page < 0) {
			page = 1;
		}
		if(rows == null || rows < 10) {
			rows = 10;
		} else if(rows > 50) {
			rows = 50;
		}
		Page<ExpenseBookDetail> pageObj = new Page(page,rows);          //1表示当前页，而10表示每页的显示显示的条目数

        QueryWrapper<ExpenseBookDetail> queryWrapper = new QueryWrapper();
		queryWrapper.eq(true,"is_enable", 1);
        
            if(id != null && !id.equals("")) {
                queryWrapper.eq(true,"id", id);
            }
            if(manageMoneyBookId != null && !manageMoneyBookId.equals("")) {
                queryWrapper.eq(true,"expense_book_id", manageMoneyBookId);
            }
            if(manageMoneyBookName != null && !manageMoneyBookName.equals("")) {
                queryWrapper.eq(true,"expense_book_name", manageMoneyBookName);
            }
            if(typeName != null && !typeName.equals("")) {
                queryWrapper.eq(true,"type_name", typeName);
            }
            if(typeId != null && !typeId.equals("")) {
                queryWrapper.eq(true,"type_id", typeId);
            }
            if(money != null && !money.equals("")) {
                queryWrapper.eq(true,"money", money);
            }
            if(remark != null && !remark.equals("")) {
                queryWrapper.eq(true,"remark", remark);
            }
            if(attachment != null && !attachment.equals("")) {
                queryWrapper.eq(true,"attachment", attachment);
            }
            if(createTime != null && !createTime.equals("")) {
                queryWrapper.eq(true,"create_time", createTime);
            }
            if(updateTime != null && !updateTime.equals("")) {
                queryWrapper.eq(true,"update_time", updateTime);
            }
            if(isEnable != null && !isEnable.equals("")) {
                queryWrapper.eq(true,"is_enable", isEnable);
            }
            if(createUserId != null && !createUserId.equals("")) {
                queryWrapper.eq(true,"create_user_id", createUserId);
            }
            if(way != null && !way.equals("")) {
                queryWrapper.eq(true,"way", way);
            }
		queryWrapper.orderByDesc("create_time");

		
		Page<ExpenseBookDetail> result = expenseBookDetailService.listByCondition(pageObj, queryWrapper);
		Gson gson = new Gson();
		String dataStr = gson.toJson(result);
		JsonElement jsonElement = JsonParser.parseString(dataStr);
		
		JsonObject jsonObject = jsonElement.getAsJsonObject();
		jsonObject.remove("isAsc");
		jsonObject.remove("limit");
		jsonObject.remove("offset");
		jsonObject.remove("openSort");
		jsonObject.remove("optimizeCountSql");
		jsonObject.remove("searchCount");
		jsonObject.add("rows", jsonObject.get("records").getAsJsonArray());
		jsonObject.remove("records");
		return jsonObject.toString();
	}
    
    @RequestMapping("list/bygroupdate")
	public Map<String,Object>  byGroupDateList(
                            String expenseBookId,
                           
                            HttpServletRequest request) {
		
    	List<Map<String, Object>> dateList = expenseBookDetailService.dateGroups(expenseBookId);
    	String date = null;
		Map<String, Object> tempMap = null;
		
		List<Map<String, Object>> resultList = new ArrayList<Map<String,Object>>();
		
		for(int index = 0; index < dateList.size(); index ++) {
			Map<String, Object> objMap = new HashMap<String,Object>();
			tempMap = dateList.get(index);
			date = tempMap.get("createDate").toString();
		
			String dateFormat = "%H:%i";
			List<Map<String, Object>> dataList = expenseBookDetailService.details(expenseBookId, date, dateFormat);
		
			objMap.put("date", date);
			objMap.put("arrays", dataList);
			resultList.add(objMap);
		}
		Gson gson = new Gson();
		String result = gson.toJson(resultList);
		System.out.println(result);
		return returnResult("200", "获取成功", result);
	}

}

