package com.hjieli.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author haojieli
 * @since 2021-01-04
 */
public class ExpenseBookDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    private String expenseBookId;

    private String expenseBookName;

    private String typeName;

    private String typeId;

    private String money;

    private String remark;

    private String attachment;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    private Integer isEnable;

    private String createUserId;

    private String way;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
  
    
    
    public String getExpenseBookId() {
		return expenseBookId;
	}

	public void setExpenseBookId(String expenseBookId) {
		this.expenseBookId = expenseBookId;
	}

	public String getExpenseBookName() {
		return expenseBookName;
	}

	public void setExpenseBookName(String expenseBookName) {
		this.expenseBookName = expenseBookName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }
    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
    public Integer getIsEnable() {
        return isEnable;
    }

    public void setIsEnable(Integer isEnable) {
        this.isEnable = isEnable;
    }
    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }
    public String getWay() {
        return way;
    }

    public void setWay(String way) {
        this.way = way;
    }

    @Override
    public String toString() {
        return "ExpenseBookDetail{" +
            "id=" + id +
            ", expenseBookId=" + expenseBookId +
            ", expenseBookName=" + expenseBookName +
            ", typeName=" + typeName +
            ", typeId=" + typeId +
            ", money=" + money +
            ", remark=" + remark +
            ", attachment=" + attachment +
            ", createTime=" + createTime +
            ", updateTime=" + updateTime +
            ", isEnable=" + isEnable +
            ", createUserId=" + createUserId +
            ", way=" + way +
        "}";
    }
}
