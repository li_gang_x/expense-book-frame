package com.hjieli.mapper;


import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.hjieli.entity.SysUserXRole;

@Mapper
public interface SysUserXRoleMapper extends BaseMapper<SysUserXRole> {

}
