package com.hjieli.mapper;


import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.hjieli.entity.ExpenseBookInfo;

@Mapper
public interface ExpenseBookInfoMapper extends BaseMapper<ExpenseBookInfo> {

}
