package com.hjieli.service;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hjieli.mapper.ExpenseBookInfoMapper;
import com.hjieli.entity.ExpenseBookInfo;

@Service("expenseBookInfoService")
public class ExpenseBookInfoService extends ServiceImpl<ExpenseBookInfoMapper, ExpenseBookInfo> {
     
	public Page<ExpenseBookInfo> listByCondition(Page<ExpenseBookInfo> page, QueryWrapper<ExpenseBookInfo> queryWrapper) {
		return  baseMapper.selectPage(page, queryWrapper);
	}
	
	
}
