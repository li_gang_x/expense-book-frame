package com.hjieli.service;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hjieli.mapper.SysRoleXDataMapper;
import com.hjieli.entity.SysRoleXData;

@Service("sysRoleXDataService")
public class SysRoleXDataService extends ServiceImpl<SysRoleXDataMapper, SysRoleXData> {
     
	public Page<SysRoleXData> listByCondition(Page<SysRoleXData> page, QueryWrapper<SysRoleXData> queryWrapper) {
		return  baseMapper.selectPage(page, queryWrapper);
	}
	
	
}
