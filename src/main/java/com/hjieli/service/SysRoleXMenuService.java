package com.hjieli.service;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hjieli.mapper.SysRoleXMenuMapper;
import com.hjieli.entity.SysRoleXMenu;

@Service("sysRoleXMenuService")
public class SysRoleXMenuService extends ServiceImpl<SysRoleXMenuMapper, SysRoleXMenu> {
     
	public Page<SysRoleXMenu> listByCondition(Page<SysRoleXMenu> page, QueryWrapper<SysRoleXMenu> queryWrapper) {
		return  baseMapper.selectPage(page, queryWrapper);
	}
	
	
}
