package com.hjieli.config;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.FileOutConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

public class SggCodeGenerator {

    public static void main(String[] args) {

        // 1、创建代码生成器
        AutoGenerator mpg = new AutoGenerator();
        
        mpg.setTemplateEngine(new FreemarkerTemplateEngine());
        

        // 2、全局配置
        GlobalConfig gc = new GlobalConfig();
        String projectPath = System.getProperty("user.dir");
        gc.setOutputDir(projectPath + "/src/main/java");
        gc.setAuthor("haojieli");
        gc.setOpen(true); //生成后是否打开资源管理器
        gc.setFileOverride(false); //重新生成时文件是否覆盖
        gc.setServiceName("%sService"); //去掉Service接口的首字母I
        gc.setIdType(IdType.ASSIGN_UUID); //主键策略
        gc.setBaseResultMap(true);
        gc.setBaseColumnList(true);
        
        gc.setDateType(DateType.TIME_PACK);//定义生成的实体类中日期类型
//        gc.setSwagger2(false);//开启Swagger2模式

        mpg.setGlobalConfig(gc);

        // 3、数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl("jdbc:mysql://localhost:3306/expense-book-db?serverTimezone=GMT%2B8");
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("Haojieli8");
        dsc.setDbType(DbType.MYSQL);
        mpg.setDataSource(dsc);

        // 4、包配置
        PackageConfig pc = new PackageConfig();
       // pc.setModuleName(""); //模块名
        pc.setParent("com.hjieli");
        pc.setController("controller");
        pc.setEntity("entity");
        pc.setService("service");
        pc.setMapper("mapper");
        mpg.setPackageInfo(pc);
        
     // 自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };

        // 自定义controller的代码模板
        String templatePath = "/templates/codeGen/Controller.java.ftlh";
        // 自定义输出配置
        List<FileOutConfig> focList = new ArrayList<>();
        // 自定义配置会被优先输出
        focList.add(new FileOutConfig(templatePath) {
         
			@Override
			public String outputFile(TableInfo tableInfo) {
				// TODO Auto-generated method stub
				  String expand = projectPath + "/src/main/java/com/hjieli/controller/api";
	              String entityFile = String.format((expand + File.separator + "%s" + ".java"), tableInfo.getControllerName());
	              return entityFile;
			}
         });
        

        // 自定义service的代码模板
        templatePath = "/templates/codeGen/Service.java.ftlh";
        // 自定义配置会被优先输出
        focList.add(new FileOutConfig(templatePath) {
         
			@Override
			public String outputFile(TableInfo tableInfo) {
				// TODO Auto-generated method stub
				  String expand = projectPath + "/src/main/java/com/hjieli/service";
	              String entityFile = String.format((expand + File.separator + "%s" + ".java"), tableInfo.getServiceName());
	              return entityFile;
			}
         });

        // 自定义mapper的代码模板
        templatePath = "/templates/codeGen/Mapper.java.ftlh";
        // 自定义配置会被优先输出
        focList.add(new FileOutConfig(templatePath) {
         
			@Override
			public String outputFile(TableInfo tableInfo) {
				// TODO Auto-generated method stub
				  String expand = projectPath + "/src/main/java/com/hjieli/mapper";
	              String entityFile = String.format((expand + File.separator + "%s" + ".java"), tableInfo.getMapperName());
	              return entityFile;
			}
         });
        
//        // 自定义mapperxml的代码模板
//        templatePath = "/templates/codeGen/MapperXML.java.ftlh";
//        // 自定义配置会被优先输出
//        focList.add(new FileOutConfig(templatePath) {
//         
//			@Override
//			public String outputFile(TableInfo tableInfo) {
//				// TODO Auto-generated method stub
//				  String expand = projectPath + "/src/main/java/com/hjieli/gen/xml";
//	              String entityFile = String.format((expand + File.separator + "%s" + ".xml"), tableInfo.getXmlName());
//	              return entityFile;
//			}
//         });
      

        // 自定义list的代码模板
        templatePath = "/templates/codeGen/list-page.java.ftlh";
        // 自定义配置会被优先输出
        focList.add(new FileOutConfig(templatePath) {
         
			@Override
			public String outputFile(TableInfo tableInfo) {
				// TODO Auto-generated method stub
				  String expand = projectPath + "/src/main/java/com/hjieli/gen/html";
	                String entityFile = String.format((expand + File.separator + "%s" + ".ftlh"), lowerFirst(tableInfo.getServiceName().replace("Service", "")) + "/list");
	                       return entityFile;
			}
         });

        // 自定义view的代码模板
        templatePath = "/templates/codeGen/view-page.java.ftlh";
        // 自定义配置会被优先输出
        focList.add(new FileOutConfig(templatePath) {
         
			@Override
			public String outputFile(TableInfo tableInfo) {
				// TODO Auto-generated method stub
				  String expand = projectPath + "/src/main/java/com/hjieli/gen/html";
	              String entityFile = String.format((expand + File.separator + "%s" + ".ftlh"), lowerFirst(tableInfo.getServiceName().replace("Service", "")) + "/view");
	              return entityFile;
			}
         });

        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);
        
        // 5、策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setInclude("expense_book_info,expense_book_detail".split(","));//对那一张表生成代码,users
      
        strategy.setNaming(NamingStrategy.underline_to_camel);//数据库表映射到实体的命名策略
//        strategy.setTablePrefix(pc.getModuleName() + "_"); //生成实体时去掉表前缀

        strategy.setColumnNaming(NamingStrategy.underline_to_camel);//数据库表字段映射到实体的命名策略
        strategy.setEntityLombokModel(false); // lombok 模型 @Accessors(chain = true) setter链式操作

        strategy.setRestControllerStyle(true); //restful api风格控制器
        strategy.setControllerMappingHyphenStyle(true); //url中驼峰转连字符
        
        
        mpg.setStrategy(strategy);
        

        // 6、执行
        mpg.execute();
    }
    
    public static String lowerFirst(String name) {
    	char [] chars = name.toCharArray();
    	chars[0] += 32;
    	return String.valueOf(chars);
    }

}