package com.hjieli.config.security.oauth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.header.HeaderWriterFilter;

import com.hjieli.config.filter.LoginFilter;


@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter{

	@Autowired
	AuthExceptionEntryPoint authExceptionEntryPoint;

	@Autowired
	AccessDeniedHandler accessDeniedHandler;
	
	@Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
		// TODO Auto-generated method stub
		resources.resourceId("resourceIds");
		resources.authenticationEntryPoint(authExceptionEntryPoint)
         			.accessDeniedHandler(accessDeniedHandler);
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		// TODO Auto-generated method stub
		
		 http.addFilterAfter(new LoginFilter(), HeaderWriterFilter.class).authorizeRequests()
		 .antMatchers(
         		"/login",
        		"/wxlogin",
        		"/api/weixin/authorize",
        		"/api/weixin/getOpenId",
        		"/api/weixin/getJsConfig",
        		"/api/carInfo/info",
         		"/error/**").permitAll()
	         .antMatchers("/admin/**").hasRole("SUPER_MGR")
             .antMatchers("/api/**").hasAnyRole("SUPER_MGR,USER,DATA_MGR")
	         .anyRequest().permitAll();
//		 http.exceptionHandling().authenticationEntryPoint(new AuthExceptionEntryPoint()) .accessDeniedHandler(new AccessDeineHandlerConfig());
		 http.headers().frameOptions().disable();

	}

	
}
