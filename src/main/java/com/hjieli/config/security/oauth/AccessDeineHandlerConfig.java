package com.hjieli.config.security.oauth;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

@Component
public class AccessDeineHandlerConfig implements AccessDeniedHandler {

	@Override
	public void handle(HttpServletRequest request, HttpServletResponse response,
			AccessDeniedException accessDeniedException) throws IOException, ServletException {
		// TODO Auto-generated method stub
//		response.setCharacterEncoding("utf-8");
//        
		response.setContentType("text/javascript;charset=utf-8");
//		response.getWriter().print("无访问权限，请先登录");
		response.sendRedirect("/login");
	}

}
