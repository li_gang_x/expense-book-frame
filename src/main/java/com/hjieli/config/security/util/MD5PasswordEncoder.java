package com.hjieli.config.security.util;

import java.security.MessageDigest;
import java.util.Random;

import org.springframework.security.crypto.codec.Hex;
import org.springframework.security.crypto.password.PasswordEncoder;

public class MD5PasswordEncoder implements PasswordEncoder{

	@Override
	public String encode(CharSequence rawPassword) {
		// TODO Auto-generated method stub
		 Random random = new Random();
         StringBuilder sBuilder = new StringBuilder(16);
         sBuilder.append(random.nextInt(9999999)).append(random.nextInt(9999999));
         int len = sBuilder.length();
         if (len < 16) {
        	 for (int i = 0; i < 16 - len; i++) {
        		 sBuilder.append("0");
        	 }
         }
		 String Salt = sBuilder. toString() ;
         rawPassword = md5Hex(rawPassword + Salt);
         char[] cs= new char [48];
         for (int i = 0; i < 48; i += 3){
        	 cs[i] = rawPassword. charAt(i/3 *2);
             char c = Salt. charAt(i / 3);
             cs[i+1] = c;
             cs[i + 2] = rawPassword. charAt(i/3*2+1);
             
         }
         return String.valueOf(cs);
	}

	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword) {
		// TODO Auto-generated method stub
		char[] cs1 = new char [32];
		char[] cs2 = new char[16];
		for (int i = 0; i < 48; i += 3){
			cs1[i / 3* 2] = encodedPassword. charAt (i);
			cs1[i /3*2+1] = encodedPassword. charAt(i + 2);
			cs2[i / 3] = encodedPassword. charAt(i + 1);
		}
		String Salt = new String (cs2) ;
		return md5Hex(rawPassword + Salt).equals(String. valueOf(cs1));
	}

	private CharSequence md5Hex(String string) {
		// TODO Auto-generated method stub

		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] digest = md.digest(string.getBytes());
			return new String (new Hex().encode(digest));
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println(e.toString());
			return "";
		}
	}
	
	public static void main(String[] args) {
		System.out.println(new MD5PasswordEncoder().encode("44f188a0586aa3ce3520432aa5e77d396e2c997505b08f0f"));
	}

}
