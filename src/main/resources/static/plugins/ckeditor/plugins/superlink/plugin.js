(function() {
    //Section 1 : 按下自定义按钮时执行的代码
    var a = {
            exec: function(editor) {
                $('#hjliWin').modal('show');
            }
        },
        //Section 2 : 创建自定义按钮、绑定方法
        b = 'superlink';
    CKEDITOR.plugins.add(b, {
        init: function(editor) {
            editor.addCommand(b, a);
            editor.ui.addButton('superlink', {
                label: '添加附件', //鼠标移上去后显示的内容
                icon: this.path + 'icon-super.png',
                command: b
            });
        }
    });
})();